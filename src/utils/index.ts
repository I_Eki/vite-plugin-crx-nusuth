import {
  existsSync,
  readdirSync,
  readFileSync,
  rmdirSync,
  rmSync,
  statSync,
} from 'fs';
import { resolve } from 'path';
import { ENTRY_EXTS } from '../constants';
import { NusuthModuleInfo } from '../types';

/**
 * 从文件路径中读取模块名称
 * @param {String} root 配置根目录
 * @param {String} path 文件路径字符串
 */
export function getNusuthModuleInfo(
  root: string,
  path: string,
): NusuthModuleInfo | null {
  const re = new RegExp(`([^\/]+)\/[^\/]+\.(${ENTRY_EXTS.join('|')})$`);
  const matches = path.match(re);
  if (!matches) return null;
  return {
    name: matches[1],
    ext: matches[2],
    path,
    relativePath: resolveChromePath(resolve(path).replace(root, '')),
  };
}

/**
 * 从文件路径获取后缀名
 * @param path 路径
 */
export function getExtension(path: string): string {
  return path.match(/(?<=\.)\S+$/)?.[0] || '';
}

/** 深度合并对象属性 */
export function deepMerge(src: any, base: any) {
  const target = JSON.parse(JSON.stringify(base));

  Object.getOwnPropertyNames(src).forEach((key) => {
    if (!base[key] || typeof base[key] !== typeof src[key]) {
      target[key] = src[key];
    } else if (base[key] instanceof Array && src[key] instanceof Array) {
      target[key] = [...new Set([...base[key], ...src[key]])];
    } else if (typeof src[key] === 'object' && typeof base[key] === 'object') {
      target[key] = deepMerge(src[key], base[key]);
    } else {
      target[key] = key in src ? src[key] : base[key];
    }
  });

  return target;
}

/**
 *获取当前是否为开发环境
 * @returns {boolean} 是否为开发环境
 */
export function isDev() {
  return process.env.NODE_ENV !== 'production';
}

/** 转换路径字符串为chrome extension的路径格式 */
export function resolveChromePath(path: string) {
  return './' + path.replace(/\\/g, '/').replace(/^[\\\/\.]+/, '');
}

/**
 * 删除目录及文件
 * @param {string} path 文件夹路径
 * @param {boolean} isRemoveSelf 是否删除自身，为否则只删除子目录及目录下文件
 */
export function removeDir(path: string, isRemoveSelf = false) {
  readdirSync(path).forEach((p) => {
    p = resolve(path, p);
    const stat = statSync(p);
    if (stat.isDirectory()) {
      removeDir(p, true);
    } else {
      rmSync(p);
    }
  });
  if (isRemoveSelf) rmdirSync(path);
}

/**
 * 根据html入口文件地址获取对应的js入口文件地址
 * @param path html入口文件地址
 */
export function getEntryInHTML(path: string, root: string): string {
  const htmlText = readFileSync(path).toString('utf-8');
  let entry = htmlText.match(
    /<body>[\s\S]+?<script[^<>]+?src="([^<>"]+)"/,
  )?.[1];

  if (!entry) {
    ENTRY_EXTS.some((ext) => {
      if (ext === 'html') return;
      const entryPath = path.replace(/html$/, ext);
      if (!existsSync(entryPath)) return;
      entry = entryPath;
      return true;
    });
  } else {
    entry = resolve(root, resolveChromePath(entry));
  }

  return entry || '';
}

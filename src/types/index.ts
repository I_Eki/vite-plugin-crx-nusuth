/** 模块信息 */
export interface NusuthModuleInfo {
  /** 模块名称 */
  name: string;

  /** 模块入口后缀 */
  ext: string;

  /** 模块入口文件路径 */
  path: string;

  /** 模块入口文件相对于config中根目录的路径 */
  relativePath: string;
}

export interface NusuthConfig {
  root?: string;
  output?: string;
  assetsDirName?: string;
}

export enum SpecialModules {
  BACKGROUND = 'background',
  OPTIONS = 'options',
  BROWSER_ACTION = 'browserAction',
  PAGE_ACTION = 'pageAction',
  DEVTOOLS = 'devtools',
  OVERRIDES = 'overrides',
}

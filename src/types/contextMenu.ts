export type ContextType =
  | 'all'
  | 'page'
  | 'frame'
  | 'selection'
  | 'link'
  | 'editable'
  | 'image'
  | 'video'
  | 'audio'
  | 'launcher'
  | 'browser_action'
  | 'page_action'
  | 'action';

export type ItemType = 'normal' | 'checkbox' | 'radio' | 'separator';

export interface CreateProperties extends UpdateProperties {
  id?: string;
}

export interface UpdateProperties {
  checked?: boolean;
  contexts?: ContextType[];
  documentUrlPatterns?: string;
  enabled?: boolean;
  id?: string;
  parentId?: string;
  targetUrlPatterns?: string;
  title?: string;
  type?: ItemType;
  visible?: boolean;
  onclick?: string;
  callback?: string;
}

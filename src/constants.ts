// 入口文件后缀格式
export const ENTRY_EXTS = ['html', 'js', 'jsx', 'ts', 'tsx'];

// 解析js中yaml配置的正则判断表达式
export const YAML_RE = /^\s*`[\r\n]([\S\s]+?)[\r\n]`;?\s+/;

import { resolve } from 'path';
import { writeFileSync } from 'fs';
import { Plugin } from 'vite';
import { InputOption } from 'rollup';
import { getEntryInHTML, isDev } from './utils';
import { NusuthModuleInfo } from './types';
import baseManifest from './manifest';
import { generateEntryYaml } from './lib/transform';
import {
  generateManifestEntries,
  getManifest,
  getManifestEntries,
  getModuleEntries,
} from './lib/manifest';
import { generateHotReload } from './lib/hotReload';

const PLUGIN_NAME = 'rollup-plugin-crx-nusuth';

export function nusuthPlugin(): Plugin {
  let manifest = baseManifest();
  let root = process.cwd();
  let outDir = 'dist';
  let isDevMode = isDev();
  let entries: NusuthModuleInfo[] = [];
  const entryJSPaths: string[] = [];

  const entryJSFilter = (id: string) => {
    return entryJSPaths.some((entry) => entry === resolve(id));
  };

  return {
    name: PLUGIN_NAME,
    apply: 'build',
    enforce: 'pre',
    config(config, { mode }) {
      const input: InputOption = {};
      if (config.root) root = config.root;
      if (config.build?.outDir) outDir = config.build.outDir;

      manifest = getManifest(root);
      entries.push(...getManifestEntries(manifest, root));

      if (entries.length < 1) {
        const moduleEntries = getModuleEntries();
        entries.push(...moduleEntries);
        moduleEntries.forEach((entry) => {
          const chromeEntryPath = entry.relativePath;
          const jsEntryPath = getEntryInHTML(
            resolve(root, chromeEntryPath),
            root,
          );
          if (jsEntryPath) entryJSPaths.push(jsEntryPath);
        });
      }

      entries.forEach((entry) => {
        input[entry.name] = entry.path;
      });

      return {
        server: {
          // 禁用开发热更新服务
          hmr: false,
        },
        build: {
          // 开发时取消混淆，方便调试
          minify: !isDevMode && 'esbuild',

          // 开发模式打开rollup自带的文件更改监视
          // TODO 自带watch只监视有直接导入关系的文件，考虑更换为chokidar监视
          watch: mode !== 'production' && {},

          // 添加入口
          rollupOptions: {
            input,
          },
        },
      };
    },
    watchChange() {
      // 监听更改时初始化manifest配置
      manifest = getManifest(root);
    },
    async transform(code: string, id: string) {
      let transformedCode = '';

      if (entryJSFilter(id)) {
        transformedCode = generateEntryYaml(code, manifest, root, id);
      }

      return transformedCode || code;
    },
    writeBundle(_, bundle) {
      generateManifestEntries(manifest, bundle, root);
      generateHotReload(manifest, root, outDir);
      writeFileSync(
        resolve(root, outDir, './manifest.json'),
        JSON.stringify(manifest, null, isDevMode ? 2 : 0),
      );
    },
  };
}

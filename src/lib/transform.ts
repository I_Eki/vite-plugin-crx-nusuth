import { deepMerge, getNusuthModuleInfo } from '../utils';
import { YAML_RE } from '../constants';
import {
  FullPermission,
  Manifest,
  OptionalPermission,
} from '../types/manifest';
import jsYaml from 'js-yaml';
import { NusuthModuleInfo, SpecialModules } from '../types';
import { CreateProperties } from '../types/contextMenu';

export function generateEntryYaml(
  code: string,
  manifest: Manifest,
  root: string,
  id: string,
): string {
  if (!YAML_RE.test(code)) return code;

  let transformedCode = '';
  const yaml = code.match(YAML_RE)![1];
  const yamlObj = jsYaml.load(yaml);
  const module = getNusuthModuleInfo(root, id)!;

  switch (module.name) {
    case SpecialModules.BACKGROUND:
      transformedCode = resolveContextMenu(code, yamlObj, manifest);
      resolveBackground(manifest, yamlObj);
      break;
    case SpecialModules.BROWSER_ACTION:
    case SpecialModules.PAGE_ACTION:
      resolveActionPage(manifest, yamlObj, module.name);
      break;
    case SpecialModules.OPTIONS:
      resolveOptionsUI(manifest, yamlObj);
      break;
    default:
      resolveContentScripts(manifest, yamlObj, module);
      break;
  }

  // 合并权限
  resolvePermission(manifest, yamlObj);

  return transformedCode;
}

/** 向全局manifest中添加指定的权限 */
export function addPermission(
  manifest: Manifest,
  permissions: FullPermission | FullPermission[],
) {
  if (typeof permissions === 'string') {
    if (!manifest.permissions!.includes(permissions)) {
      manifest.permissions!.push(permissions);
    }
  } else {
    permissions.forEach((item) => {
      if (!manifest.permissions!.includes(item)) {
        manifest.permissions!.push(item);
      }
    });
  }
}

/**
 * 从yamlInJS配置中解析菜单配置
 * @param code 源代码字符串
 * @param yamlObj yaml配置对象
 * @param manifest manifest配置对象
 */
export function resolveContextMenu(
  code: string,
  yamlObj: any,
  manifest: Manifest,
): string {
  if (!yamlObj.contextMenus) return code;

  const menuCodes = yamlObj.contextMenus.map((menu: CreateProperties) => {
    const { callback, onclick, ...rest } = menu;
    let menuCode = '';

    const configCode = JSON.stringify(rest).replace(/^{|}$/g, '');

    menuCode = `chrome.contextMenus.create({${configCode}}`;

    if (callback) menuCode += `, ${callback}`;
    menuCode += ');';

    if (onclick)
      menuCode += `\nchrome.contextMenus.onClicked.addListener(${onclick});`;

    return `\n${menuCode}\n`;
  });

  if (menuCodes.length > 0) {
    const installCode = `
      chrome.runtime.onInstalled.addListener(function() {
        ${menuCodes.join('')}
      });
    `;

    code += installCode;
    addPermission(manifest, [OptionalPermission.CONTEXT_MENUS]);
  }

  return code;
}

/** 处理模块所需权限 */
export function resolvePermission(manifest: Manifest, yamlObj: any) {
  ['permissions', 'optional_permissions'].forEach((key) => {
    if (!manifest[key]) {
      manifest[key] = [];
    }

    if (yamlObj[key]) {
      if (typeof yamlObj[key] === 'string') {
        manifest[key].push(yamlObj[key]);
      } else if (yamlObj[key] instanceof Array) {
        manifest[key].push(...yamlObj[key]);
      }
      delete yamlObj[key];
    }
  });
}

/** 解析background独有配置 */
export function resolveBackground(manifest: Manifest, yamlObj: any) {
  const config: any = {
    persistent: yamlObj.persistent || false,
  };

  if (!manifest.background?.page && yamlObj.scripts) {
    config.scripts = yamlObj.scripts;
  }

  manifest.background = deepMerge(config, manifest.background || {});
}

/** 解析brower_action和page_action独有配置 */
export function resolveActionPage(
  manifest: Manifest,
  yamlObj: any,
  key: string,
) {
  const config: any = {};
  const configKeys = ['default_icon', 'default_title'];

  configKeys.forEach((cfk) => {
    if (yamlObj[cfk]) {
      config[cfk] = yamlObj[cfk];
    }
  });

  manifest[key] = deepMerge(config, manifest[key] || {});
}

/** 解析content_scripts独有配置 */
export function resolveContentScripts(
  manifest: Manifest,
  yamlObj: any,
  module: NusuthModuleInfo,
) {
  const config: any = {};
  const configKeys = [
    'matches',
    'css',
    'js',
    'match_about_blank',
    'exclude_matches',
    'include_globs',
    'exclude_globs',
    'run_at',
    'all_frames',
  ];

  configKeys.forEach((cfk) => {
    if (yamlObj[cfk]) {
      config[cfk] = yamlObj[cfk];
    }
  });

  if (!manifest.content_scripts) {
    manifest.content_scripts = [];
  }

  config.name = module.name;

  manifest.content_scripts.push(config);
}

/** 解析options_ui独有配置 */
export function resolveOptionsUI(manifest: Manifest, yamlObj: any) {
  const config: any = {};
  const configKeys = ['open_in_tab'];

  configKeys.forEach((cfk) => {
    if (yamlObj[cfk]) {
      config[cfk] = yamlObj[cfk];
    }
  });

  manifest.options_ui = deepMerge(config, manifest.options_ui || {});
}

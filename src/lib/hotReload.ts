import { copyFileSync, existsSync, rmSync } from 'fs';
import { resolve } from 'path';
import { Manifest } from '../types/manifest';
import { deepMerge, isDev } from '../utils';

/**
 * 实现非生产模式下热重载所需要的静态文件处理
 * @param manifest manifest配置
 * @param root 根目录
 * @param outDir 项目输出目录
 */
export function generateHotReload(
  manifest: Manifest,
  root: string,
  outDir: string,
) {
  const hotReloadFileName = 'hot-reload.js';
  const outPath = resolve(root, outDir, hotReloadFileName);
  const hasExisted = existsSync(outPath);

  const isDevMode = isDev();
  if (isDevMode) {
    if (hasExisted) return;
    const srcPath = resolve(__dirname, '../statics/hot-reload.js');
    copyFileSync(srcPath, outPath);
    manifest.background = deepMerge(
      {
        scripts: [hotReloadFileName],
      },
      manifest.background || {},
    );
  } else {
    if (hasExisted) rmSync(outPath);
  }
}

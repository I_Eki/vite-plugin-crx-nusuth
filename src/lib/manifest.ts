import { existsSync, readdirSync, statSync } from 'fs';
import {
  deepMerge,
  getExtension,
  getNusuthModuleInfo,
  resolveChromePath,
} from '../utils';
import { Manifest, OptionalPermission } from '../types/manifest';
import { ENTRY_EXTS } from '../constants';
import { resolve } from 'path';
import baseManifest from '../manifest';
import { NusuthModuleInfo, SpecialModules } from '../types';
import { OutputBundle, OutputChunk } from 'rollup';
import { addPermission } from './transform';

/**
 * 读取指定根目录下的manifest.json配置
 * @param root 根目录
 */
export function getProjManifest(root: string): Manifest | null {
  const exts = ['json', 'js'];
  let manifest: Manifest | null = null;

  [resolve(root, 'src'), root].some((rootPath) => {
    return exts.some((ext) => {
      const mfPath = resolve(rootPath, `./manifest.${ext}`);
      if (!existsSync(mfPath)) return;
      manifest = require(mfPath);
      return true;
    });
  });

  return manifest;
}

/**
 * 获取整合后的manifest.json配置
 * @param root 根目录
 * @returns
 */
export function getManifest(root: string): Manifest {
  const manifest: Manifest = baseManifest();
  const projMf = getProjManifest(root);
  return projMf ? deepMerge(projMf, manifest) : manifest;
}

/**
 * 从manifest.json中读取入口列表
 * @param manifest manifest.json配置
 * @param root 根目录
 */
export function getManifestEntries(
  manifest: Manifest,
  root: string,
): NusuthModuleInfo[] {
  let paths: string[] = [];
  let entries: NusuthModuleInfo[] = [];

  // background
  [manifest.background?.page, manifest.background?.scripts?.[0]].some(
    (bgEntry) => {
      if (!bgEntry) return;
      entries.push({
        name: SpecialModules.BACKGROUND,
        ext: getExtension(bgEntry),
        path: resolve(root, bgEntry),
        relativePath: bgEntry,
      });
      return true;
    },
  );

  // content_scripts
  if (manifest.content_scripts) {
    manifest.content_scripts.forEach((item) => {
      paths.push(...(item.js || []));
    });
  }

  paths.forEach((path) => {
    const entryInfo = getNusuthModuleInfo(root, resolve(root, path));
    entryInfo && entries.push(entryInfo);
  });

  return entries;
}

/**
 * 从约定式目录中获取入口列表
 * @param moduleRoot 模块路径
 */
export function getModuleEntries(
  moduleRoot: string = resolve(process.cwd(), 'src/modules'),
): NusuthModuleInfo[] {
  let entries: NusuthModuleInfo[] = [];
  if (!existsSync(moduleRoot)) return entries;
  const children = readdirSync(moduleRoot);
  children.forEach((child) => {
    const cp = resolve(moduleRoot, child);
    const stat = statSync(cp);

    if (stat.isDirectory()) {
      ENTRY_EXTS.some((ext) => {
        const entryPath = resolve(cp, `index.${ext}`);

        if (!existsSync(entryPath)) return;
        entries.push({
          name: child,
          ext,
          path: entryPath,
          relativePath: resolveChromePath(entryPath.replace(process.cwd(), '')),
        });

        return true;
      });
    }
  });

  return entries;
}

export function generateManifestEntries(
  manifest: Manifest,
  bundle: OutputBundle,
  root: string,
) {
  Object.values(bundle).forEach((item) => {
    if (!(<any>item).isEntry) return;
    const { facadeModuleId, fileName } = item as OutputChunk;
    const module = getNusuthModuleInfo(root, facadeModuleId || '');
    if (!module) return;
    let isHTMLEntry = module.ext === 'html';
    const chromeEntryPath = resolveChromePath(
      isHTMLEntry ? module.relativePath : fileName,
    );

    switch (module.name) {
      case SpecialModules.BACKGROUND:
        if (isHTMLEntry) {
          manifest.background = {
            page: chromeEntryPath,
          };
        } else {
          manifest.background = {
            scripts: [chromeEntryPath],
          };
        }
        addPermission(manifest, OptionalPermission.BACKGROUND);
        break;
      case SpecialModules.BROWSER_ACTION:
        manifest.browser_action = {
          default_popup: chromeEntryPath,
        };
        break;
      case SpecialModules.PAGE_ACTION:
        manifest.page_action = {
          default_popup: chromeEntryPath,
        };
        break;
      case SpecialModules.OPTIONS:
        manifest.options_ui = {
          page: chromeEntryPath,
        };
        break;
      default:
        // content_scripts
        if (!manifest.content_scripts?.length) break;
        const index = manifest.content_scripts.findIndex(
          (item) => item.name === module.name,
        );
        if (!~index) break;

        manifest.content_scripts[index] = deepMerge(
          {
            js: [chromeEntryPath],
          },
          manifest.content_scripts[index],
        );

        // 打包时去除content_scripts的唯一标识
        delete manifest.content_scripts[index].name;
        addPermission(manifest, OptionalPermission.TABS);
        break;
    }
  });
}

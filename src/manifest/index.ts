import BaseManifest from './manifest.base';
import DevManifest from './manifest.dev';
import ProdManifest from './manifest.prod';
import { deepMerge, isDev } from '../utils';
import { Manifest } from '../types/manifest';

export default function (): Manifest {
  let manifest = BaseManifest();

  if (isDev()) {
    const devManifest = DevManifest();

    manifest = deepMerge(devManifest, manifest);
  } else {
    const prodManifest = ProdManifest();

    manifest = deepMerge(prodManifest, manifest);
  }

  return manifest;
}

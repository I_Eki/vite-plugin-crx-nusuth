# Nusuth

此项目基于 vuepress 建立，提供 `vite-plugin-crx-nusush` 插件的详细使用方法和参数说明。

插件基于 `Vite` 实现对 `Chrome-Extension` 项目的自动化、响应式、热重载式编译打包，支持根目录 `manifest.json` 手动配置入口和约定式目录入口两种开发解决方案。对常用必须权限和配置实现自动化解析，并且支持 `YAML-In-JS` 形式的独立配置书写和自动化解析处理，尽力实现让开发者将大部分焦点放在业务开发，而非常规配置上。

[部署地址](http://nusuth.eki.cool/)

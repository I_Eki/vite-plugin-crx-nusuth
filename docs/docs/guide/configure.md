# 配置

插件目前支持 `MV2` 和 `MV3` 的 `content_scripts` 模块编译，仅支持 `MV2` 版本的 `background` 模块编译（热重载功能相同）。

## 入口模式

插件使用 `Vite` 的多入口开发模式，为了同时满足原生扩展和模块式的开发习惯，入口的获取方式也有两种，**这两种方式获取到的入口将进行合并去重处理**。

### 配置入口

项目根目录可创建 `manifest.json` 文件，插件会从其中的 `background`、`content_scripts` 等配置项获取有效的入口相对路径（相对于 `Vite` 的 `root` 目录）。

::: details 目前支持的入口配置项
[background](https://developer.chrome.com/docs/extensions/mv2/background_pages/)

[content_scripts](https://developer.chrome.com/docs/extensions/mv2/content_scripts/)

[options_ui](https://developer.chrome.com/docs/extensions/mv2/options)
<br>↓ 与 options_ui 相同且无法配置是否在沙盒弹窗中显示，**不使用**<br>
[options_page](https://developer.chrome.com/docs/extensions/mv2/options)

↓ 以下两项二选一<br>
[browser_action](https://developer.chrome.com/docs/extensions/reference/browserAction/)
<br>[page_action](https://developer.chrome.com/docs/extensions/reference/pageAction/)
:::

### 约定式目录入口

默认目录 `root/src/modules` 为模块目录（暂无法配置），此文件夹下的所有子文件夹都被视为一个单独的模块，支持其中的 `index.(html|js|jsx|ts|tsx)` 文件作为入口文件。具体情况如下：

| 模块名称      | 对应 manifest 配置项 | 入口支持 | 独立配置项                          |
| ------------- | -------------------- | -------- | ----------------------------------- |
| background    | background           | html/js  | [background](#background)           |
| options       | options_ui           | html     | [options](#options)                 |
| browserAction | browser_action       | html     | [browser_action](#browseraction)    |
| pageAction    | page_action          | html     | [page_action](#pageaction)          |
| 其它          | content_scripts      | js       | [content_scripts](#content-scripts) |

可存在多个非关键字模块文件夹，每个模块都将作为 `content_scripts` 中的一个子项。

项目目录示例：

```:no-line-numbers
├── package.json
├── vite.config.js
└── src
    └── modules
        └── background
            └── index.js
        └── browserAction
            ├── index.html
            └── index.js
        └── demo
            └── index.js
```

## Vite 相关

### 目录

插件根目录路径和输出目录名称分别读取 `vite.config.js` 配置中的 `root` 和 `build.outDir` 属性值，如果不存在，则分别使用项目根目录 `process.cwd()` 和 `dist`。

## 插件配置

插件目前无特殊配置。

## manifest.json

插件支持在项目根目录下建立 `manifest.json` 文件，其中字段和属性按照官方文档配置即可。需要注意的是，引用文件的路径都是相对于[项目根目录](#目录)。

### 官方文档

[官方文档](https://developer.chrome.com/docs/extensions/mv2/manifest/)

### 项目配置

#### 默认配置

插件内置以下默认配置，所有属性值为简单类型的配置项都会直接覆盖，复制类型的配置项值会进行深度合并。

**注意**：插件目前对 `MV3` 支持度尚有欠缺，暂不建议更改 `manifest_version` 的值。

```json
{
  "manifest_version": 2,
  "name": "my-nusuth-crx",
  "version": "0.0.1",
  "description": "A chrome extension build by Nusuth",
  "optional_permissions": [],
  "permissions": [],
  "content_scripts": []
}
```

#### js 配置方式

如果需要更加灵活地修改扩展配置，可以在项目根目录下创建 `manifest.js` 文件，文件运行于 `nodejs` 环境，且需要导出一个配置对象，例如：

```javascript
// manifest.js

const manifest = {};

if (process.env.NODE_ENV !== 'production') {
  manifest.background = {
    scripts: ['/js/hot-reload.js'],
  };
}

module.exports = manifest;
```

## YAMLInJS

`YAMLInJS` 是为了让分散结构的 Chrome 扩展项目能够适配使用 `Vite` 入口单一文件模式进行开发，同时也能自定义模块独立配置而实现的一种将配置信息写在入口 js 文件内部的开发形式。

由于 Chrome 扩展对于不同模块具有不同的配置字段，所以插件对应不同模块也有不同的独立配置项。

### [background](https://developer.chrome.com/docs/extensions/mv2/background_pages/)

#### 简介

扩展程序是基于事件的程序，用于修改或增强 `Chrome` 浏览体验。它能够监听到一系列诸如跳转到新页面、移除书签或关闭选项卡等浏览器事件，而监听事件并完成回调等操作都需要在 `background` 脚本中完成。

浏览器默认将在需要时加载 `background` 模块，在空闲时将其卸载。能够触发加载的事件包括：

1. 扩展程序首次安装或更新到新版本；
2. `background` 脚本正在监听一个事件，并且该事件被触发；
3. `content_scripts` 或其他扩展程序向 `background` 发送消息；
4. 扩展中的另一个视图（例如 `browserAction` 等的弹窗页面）调用 API `runtime.getBackgroundPage`；

被加载后，只要 `background` 仍在执行操作（例如调用 `Chrome API` 或发出网络请求），它就会一直运行。此外，在所有扩展的可见视图和所有消息端口都关闭之前，后台页面都不会卸载。请注意，打开视图不会触发加载 `background` 页面，只会阻止它在加载后关闭。

有效的 `background` 脚本会一直处于休眠状态，直到它们监听的事件被触发，然后对指定的指令做出反应，最后被卸载。

#### 全配置示例：

```yaml:no-line-numbers
`
contextMenus:
  -
    id: test
    title: 测试
    contexts:
      - selection
    onclick: handleNameMenuClick
    callback: handleNameMenuCreateCallback
persistent:
  true
scripts:
  - ./js/hot-reload.js
`;
```

#### [contextMenus](https://developer.chrome.com/docs/extensions/reference/contextMenus)

- 类型：`Array<{ ...createProperties, onclick, callback }>`

声明扩展程序需要加载的右键菜单列表。**注意**声明式创建菜单无法控制加载和卸载时机，如果有需要，请使用原生 API 方法进行菜单的创建和销毁。

::: details contextMenus 可配置项

##### checked

- 类型：`Boolean`

设置 `radio` 类型右键菜单的初始选中状态，其它类型菜单无效。

##### contexts

- 类型：`String[]`
- 类型：`page`

菜单可触发的上下文情景，可设置多个，`launcher` 仅适用于 `Chromium OS` 中的应用，浏览器扩展无效。取值范围如下：

| 取值           | 上下文                                |
| -------------- | ------------------------------------- |
| all            | 所有情景                              |
| page           | 页面空白处                            |
| frame          | 窗口空白处(包括原网页和内部 `iframe`) |
| selection      | 选中文字                              |
| link           | 超链接                                |
| editable       | 可输入区域                            |
| image          | 图片                                  |
| video          | `video` 节点                          |
| audio          | `audio` 节点                          |
| browser_action | 浏览器扩展图标                        |
| page_action    | 页面扩展图标                          |
| action         | `browser_action` 和 `page_action`     |

##### documentUrlPatterns

- 类型：`String[]`

菜单独立的 URL 匹配规则列表，格式等同于 `content_scripts` 配置项中的 `matches` 格式。

##### enabled

- 类型：`Boolean`

菜单是否可用，若设置成 `false`，则菜单为置灰状态且不可点击。

##### id

- 类型：`String`

菜单唯一标识，进行菜单分组或 `API` 调用时需要使用，否则可不设置。

##### parentId

- 类型：`String`

父级分组菜单的唯一标识，设置后菜单将在对应菜单的右侧展开列表中显示。

##### targetUrlPatterns

- 类型：`String[]`

与 `documentUrlPatterns` 配置项类似，但此项用于筛选触发上下文为 `img`、`audio`、`video` 标签的 `src` 属性，和触发上下文为 `a` 标签的 `href` 属性。

##### title

- 类型：`String`

菜单标题。

##### type

- 类型：`String`

菜单类型，可选值如下：

| 取值      | 类型说明 |
| --------- | -------- |
| normal    | 普通菜单 |
| checkbox  | 复选框   |
| radio     | 单选框   |
| separator | 分隔线   |

##### visible

- 类型：`Boolean`

菜单是否可见。

##### onclick

- 类型：`String`

菜单点击回调函数的名称，对应函数需要在当前 js 文件中声明，参数和返回请参考官方文档。

##### callback

- 类型：`String`

菜单创建后的回调方法，对应函数需要在当前 js 文件中声明，参数和返回请参考官方文档。

:::

#### persistent

- 类型：`Boolean`
- 默认：`false`

标识 `background` 模块在被需要时加载，在空闲时被关闭，大多数情况下不需要修改。具体详情参考官方文档说明。

#### scripts

- 类型：`String[]`
- 默认：`undefined`

需要额外加载的 `background` **静态资源**模块入口文件路径（相对于 `root`）。此配置项仅当 `background` 模块入口为当前 js 文件时生效，如果用 html 文件作为入口，参考[引入](#引入)。

### [options](https://developer.chrome.com/docs/extensions/mv2/options/)

#### 简介

提供一个选项页面（或弹窗）来管理扩展程序的配置信息。用户可以通过右键单击工具栏中的扩展程序图标然后选择选项。

![扩展图标右键选择选项](/images/options_action.png)

或是在地址栏中输入 `chrome://extensions` 并回车，找到所需的扩展程序，点击并查看详细信息，然后选择选项链接来查看扩展程序的选项。

![扩展详情中选择选项](/images/options_extensions.png)

#### 全配置示例：

```yaml:no-line-numbers
`
open_in_tab: true
`;
```

#### open_in_tab

- 类型：`Boolean`
- 默认：`false`

声明 `options` 页面是否在独立的 `Tab` 页面中打开，默认会在当前页面的一个独立弹窗中打开。

### [browserAction](https://developer.chrome.com/docs/extensions/mv2/browser_action/)

#### 简介

声明此模块将在浏览器地址栏右侧新增一个可交互的图标。除了图标之外，还可以定义 `tooltip`、`badge`、`backgroundColor`、`popup页面`等属性。

**注意：** 对于本插件而言， `popup页面` 即为模块的入口页面，`badge` 和 `backgroundColor` 属性只能通过 `browserAction.setBadgeText` 和 `browserAction.setBadgeBackgroundColor` API 来设置。

#### 全配置示例：

```yaml:no-line-numbers
`
default_icon:
  images/icon16.png
# or
default_icon:
  16: images/icon16.png
  24: images/icon24.png
  32: images/icon32.png
default_title:
  Chrome Extension
`;
```

#### default_icon

- 类型：`String | Object`

扩展图标声明，为对象格式，需要用对应大小作为属性名称，对应的图标资源路径作为属性值。

也可以设置为一个字符串，对于不同大小设备都使用同一大小的图标，但会影响用户体验。

#### default_title

- 类型：`String`
- 默认：`undefined`

鼠标移入扩展图标时，展示的 Tootip 文字。

### [pageAction](https://developer.chrome.com/docs/extensions/mv2/page_action/)

#### 简介

功能等同于 `browserAction`，但只会在**当前页面**生效，且无法设置 `badge` 和 `backgroundColor`。

一般情况下会使用 API 来控制生效和置灰。

#### 全配置示例：

```yaml:no-line-numbers
`
default_icon:
  images/icon16.png
# or
default_icon:
  16: images/icon16.png
  24: images/icon24.png
  32: images/icon32.png
default_title:
  Chrome Extension
`;
```

#### default_icon

- 类型：`Object`

扩展图标声明，为对象格式，需要用对应大小作为属性名称，对应的图标资源路径作为属性值。

#### default_title

- 类型：`String`
- 默认：`undefined`

鼠标移入扩展图标时，展示的 Tootip 文字。

### [content_scripts](https://developer.chrome.com/docs/extensions/mv2/content_scripts/)

#### 简介

`content_scripts` 脚本直接运行于原网页的上下文环境中，通过使用标准的文档对象模型(DOM)，他们能够读取浏览器访问的网页的详细信息，对其进行更改并将信息传递给扩展中的其他脚本模块。

`content_scripts` 通过使用 API 传递消息给 `background`等模块进行交互和使用涉及敏感操作的 API，能够直接在 `content_scripts` 中使用的 `Chrome APIs` 有：

- [i18n](https://developer.chrome.com/docs/extensions/reference/i18n/)
- [storage](https://developer.chrome.com/docs/extensions/reference/storage/)
- [runtime](https://developer.chrome.com/docs/extensions/reference/runtime/):
  - [connect](https://developer.chrome.com/docs/extensions/reference/runtime#method-connect)
  - [getManifest](https://developer.chrome.com/docs/extensions/reference/runtime#method-getManifest)
  - [getURL](https://developer.chrome.com/docs/extensions/reference/runtime#method-getURL)
  - [id](https://developer.chrome.com/docs/extensions/reference/runtime#property-id)
  - [onConnect](https://developer.chrome.com/docs/extensions/reference/runtime#event-onConnect)
  - [onMessage](https://developer.chrome.com/docs/extensions/reference/runtime#event-onMessage)
  - [sendMessage](https://developer.chrome.com/docs/extensions/reference/runtime#method-sendMessage)

#### 全配置示例：

```yaml:no-line-numbers
`
all_frames: true
css:
  - /assets/index.css
exclude_globs:
  - '*nytimes.com/???s/*'
exclude_matches:
  - '*://*.exclude.com/*'
include_globs:
  - 'http://*'
js:
  - /js/demo.js
matches:
  - '*://*.eki.cool/*'
  - '*://*.example.com/*'
match_about_blank: true
run_at: document_idle
`;
```

#### all_frames

- 类型：`Boolean`
- 默认：`false`

声明当前脚本是否注入页面中的所有 `frame` （如页面内 `iframe` 标签）。**注意**脚本注入子级 `frame` 时也会进行网址规则判断，不满足条件则不会注入。

#### css

- 类型：`String[]`

引入当前页面脚本需要加载的**静态资源**样式文件路径（相对于 `root`)。

#### exclude_globs

- 类型：`String[]`

`include_globs` 的黑名单配置项。

#### exclude_matches

- 类型：`String[]`

`matches` 网址匹配规则黑名单。

#### include_globs

- 类型：`String[]`

与 `matches` 配置项并行的另一种匹配规则，一般不使用。

#### js

- 类型：`String[]`

引入当前页面脚本需要加载的**静态资源**脚本文件路径（相对于 `root`)。

#### matches

- 类型：`String[]`
- 是否必须：`是`

定义注入当前页面脚本时需要匹配的网址规则，**注意**此规则与正则类似，但不完全相同，可参考官方文档说明。

#### match_about_blank

- 类型：`Boolean`
- 默认：`false`

声明当前脚本是否需要注入到 `about:blank` 空白页。

#### run_at

- [官方文档链接](https://developer.chrome.com/docs/extensions/mv2/content_scripts/#run_time)
- 类型：`String`
- 取值范围：`document_idle` | `document_start` | `document_end`
- 默认：`document_idle`

声明当前页面脚本的加载时机。

| 取值           | 加载时机                                                                          |
| -------------- | --------------------------------------------------------------------------------- |
| document_start | 页面 DOM 加载之前                                                                 |
| document_end   | 页面 DOM 加载完成之后，页面资源(包括图片、iframe 等)加载之前                      |
| document_idle  | 介于 `document_end`之后 和 `window.onload` 事件完成之前，具体时机取决于页面复杂度 |

一般情况下建议使用默认值。如果脚本操作涉及页面动态 DOM 节点，建议使用[MutationObserver](https://developer.mozilla.org/zh-CN/docs/Web/API/MutationObserver)进行监听，判断操作时机。

# 快速上手

## 从 Template 项目搭建

鉴于目前尚未完成脚手架项目的编写，比较简单的方案是直接从模板项目拉取代码，开箱即用。

```sh
git clone https://I_Eki@bitbucket.org/I_Eki/nusuth-crx-template.git
```

## 手动搭建

也可以选择手动搭建一个开发项目。如果不熟悉 `Vite` 框架，建议直接从模板项目搭建。

### Vite 项目搭建

参考[搭建第一个 Vite 项目](https://vitejs.cn/guide/#scaffolding-your-first-vite-project).

### 插件安装

```bash
# install dependencies
npm i vite-plugin-crx-nusuth -D

# or use yarn
yarn add vite-plugin-crx-nusuth -D
```

### 插件使用

```javascript
// vite.config.(js|ts)

import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { nusuthPlugin } from 'vite-plugin-crx-nusuth';

export default defineConfig({
  plugins: [vue(), nusuthPlugin()],
});
```

### Hello, world!

#### 创建模块入口配置

```sh
echo {"background":{"page":"./index.html"}} > manifest.json
```

#### 填充代码

```javascript
// src/main.ts

console.log('Hello, world');
```

#### 脚本替换

`package.json`

```json
{
  "scripts": {
    "dev": "vue-tsc --noEmit && vite build --mode development",
    "build": "vue-tsc --noEmit && vite build",
    "preview": "vite preview"
  }
}
```

## 项目运行

```sh
# dev
npm run dev

# or use yarn
yarn dev

# build
npm run build

# or use yarn
yarn build
```

## 在浏览器中加载脚本

在 Chrome 浏览器地址栏中输入 `chrome://extensions/` 打开扩展列表页面。首先确保右上角开发模式处于打开状态，然后点击左边 `加载已解压的扩展程序` 按钮。

![Chrome扩展页面](/images/chrome_extension.png)

选中项目根目录的打包目录（默认为 `root/dist`），点击加载。提示加载成功后，点击扩展程序卡片的 `index.html` ，查看 `console` 选项卡即可看到代码效果。

![扩展卡片](/images/extension_card.png)

![consoleTab](/images/console_tab.png)

## 实现热重载

`v0.0.5` 以上版本已自动处理热重载，但限制了 `background` 模块的入口文件只能是 `js` 类型。

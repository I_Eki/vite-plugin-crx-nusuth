class Coord {
  public X: number;
  public Y: number;

  constructor(x: number = 0, y: number = 0) {
    this.X = x;
    this.Y = y;
  }
}

function createRandomPoint(width: number, height: number) {
  return new Coord(Math.random() * width, Math.random() * height);
}

function getBezierPoint(start: Coord, end: Coord, t: number): Coord {
  const dX = end.X - start.X;
  const dY = end.Y - start.Y;

  return new Coord(start.X + dX * t, start.Y + dY * t);
}

export class BezierDiscreteCurve {
  private ctx: CanvasRenderingContext2D;
  private width: number;
  private height: number;
  private len: number;
  private indexOnSecondLine: number;
  private t: number;
  private points: Coord[];
  private line: string;
  private ms: number = 50;
  private timer: NodeJS.Timer | null = null;
  private isPaused: boolean = false;
  private cellSize: number = 16;

  constructor(ctx: CanvasRenderingContext2D, line: string, width: number, height: number, len: number, ms?: number) {
    this.ctx = ctx;
    this.line = line;
    this.width = width;
    this.height = height;
    this.len = len;
    this.ms = ms || 50;
    this.indexOnSecondLine = 0;
    this.t = 1 / (len - 1);
    this.points = Array(4)
      .fill(0)
      .map(i => createRandomPoint(width, height));
    this.points.unshift(new Coord(0, height));

    this.timer = setInterval(() => this.stepRender(), this.ms);
  }

  switchPause() {
    if (this.isPaused) {
      this.timer = setInterval(() => this.stepRender(), this.ms);
    } else {
      this.stop();
    }
    this.isPaused = !this.isPaused;
  }

  stop() {
    if (this.timer) clearInterval(this.timer);
  }

  next() {
    if (this.indexOnSecondLine === this.len) {
      this.indexOnSecondLine = 0;
      this.points.splice(0, 2);
      this.points.push(createRandomPoint(this.width, this.height));
      this.points.push(createRandomPoint(this.width, this.height));
    }

    const line0: Coord[] = [];
    const line1: Coord[] = [];

    for (let i = 0; i < this.len; i++) {
      const t = this.t * i;

      if (i < this.indexOnSecondLine) {
        const p0 = getBezierPoint(this.points[2], this.points[3], t);
        const p1 = getBezierPoint(this.points[3], this.points[4], t);
        line0.unshift(getBezierPoint(p0, p1, t));
      } else {
        const p0 = getBezierPoint(this.points[0], this.points[1], t);
        const p1 = getBezierPoint(this.points[1], this.points[2], t);
        line1.unshift(getBezierPoint(p0, p1, t));
      }
    }

    this.indexOnSecondLine++;
    return [...line0, ...line1];
  }

  stepRender() {
    this.ctx.fillStyle = 'rgb(0,0,0)';
    this.ctx.fillRect(0, 0, this.width, this.height);

    const points = this.next();

    points.forEach((point, index) => {
      if (this.line[index] === '0') {
        this.renderZero(point.X, point.Y);
      } else {
        this.renderOne(point.X, point.Y);
      }
    });
  }

  renderZero(x: number, y: number) {
    this.ctx.fillStyle = 'rgb(255,255,255)';
    const halfSize = this.cellSize / 2;
    this.ctx.fillRect(x - halfSize, y - halfSize, this.cellSize, this.cellSize);

    this.ctx.fillStyle = 'rgb(0,0,0)';
    this.ctx.beginPath();
    this.ctx.moveTo(x, y);
    this.ctx.arc(x, y, halfSize * 0.8, 0, Math.PI * 2, true);
    this.ctx.fill();
  }

  renderOne(x: number, y: number) {
    this.ctx.fillStyle = 'rgb(255,255,255)';
    this.ctx.beginPath();
    this.ctx.moveTo(x, y);
    this.ctx.arc(x, y, this.cellSize / 2, 0, Math.PI * 2, true);
    this.ctx.fill();
  }
}

export default {
  locales: {
    '/': {
      lang: 'zh-CN',
      title: 'Nusuth',
      description: 'Nusuth 基于Vite的Chrome扩展热重载开发插件',
    },
    '/en/': {
      lang: 'en-US',
      title: 'Nusuth',
      description: 'Nusuth-a vite plugin for bunding Chrome-Extension project.',
    },
  },
  themeConfig: {
    repo: 'https://bitbucket.org/I_Eki/vite-plugin-crx-nusuth/',
    editLink: false,
    locales: {
      '/': {
        selectLanguageName: '简体中文',
        selectLanguageText: '选择语言',
        navbar: [
          {
            text: '指南',
            link: '/guide/',
          },
          {
            text: '参考',
            children: [
              {
                text: '官方文档',
                children: [
                  {
                    text: 'MV2配置列表',
                    link: 'https://developer.chrome.com/docs/extensions/mv2/manifest/',
                  },
                  {
                    text: 'Firefox扩展官方文档',
                    link: 'https://developer.mozilla.org/zh-CN/docs/Mozilla/Add-ons/WebExtensions',
                  },
                ],
              },
              {
                text: 'Blog',
                children: [
                  {
                    text: 'Chrome扩展入门',
                    link: 'https://www.cnblogs.com/liuxianan/p/chrome-plugin-develop.html',
                  },
                  {
                    text: '热重载',
                    link: 'http://eki.cool/code/chrome-extension/热重载.html',
                  },
                  {
                    text: 'yaml语法简介',
                    link: 'https://www.ruanyifeng.com/blog/2016/07/yaml.html',
                  },
                ],
              },
            ],
          },
          {
            text: '更新日志',
            link: '/CHANGELOG.md',
          },
        ],
        sidebar: {
          '/guide/': ['index', 'getting-start', 'configure'].map((name) => `/guide/${name}.md`),
        },
      },
      '/en/': {
        selectLanguageName: 'English',
        selectLanguageText: 'Languages',
        navbar: [
          {
            text: 'Guide',
            link: 'guide',
          },
          {
            text: 'Reference',
            children: [
              {
                text: 'Official document',
                children: [
                  {
                    text: 'Manifest version 2 file format',
                    link: 'https://developer.chrome.com/docs/extensions/mv2/manifest/',
                  },
                ],
              },
              {
                text: 'Blog',
                children: [
                  {
                    text: 'yaml',
                    link: 'https://yaml.org/',
                  },
                ],
              },
            ],
          },
        ],
      },
    },
    lastUpdated: 'Last Updated',
  },
  plugins: [
    [
      '@vuepress/plugin-search',
      {
        locales: {
          '/': {
            placeholder: '搜索',
          },
          '/en/': {
            placeholder: 'Search',
          },
        },
      },
    ],
  ],
};

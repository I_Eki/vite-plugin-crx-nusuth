import { defineClientAppEnhance } from '@vuepress/client';
import ReadMeBody from './components/ReadMeBody/index.vue';

export default defineClientAppEnhance(({ app, router, siteData }) => {
  app.component('ReadMeBody', ReadMeBody);
});

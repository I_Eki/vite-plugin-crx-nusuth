- **v0.0.7** 22.01.10 `fix`

1. 修改 package.json 中 files 配置错误导致打包未包含静态资源文件的问题。
2. 修改默认开发配置导致将热重载模块当成入口的问题。
3. 修改开发模式热重载后 manifest 未刷新导致 content_scripts 配置错误，找不到入口的问题。
4. 添加 release-it 和修改提交文案模板，实现自动化版本更新推送和管理。
5. 修改 deepMerge 方法中非空值逻辑错误。

- **v0.0.6** 22.01.07 `fix`

1. 修改 package.json 配置未包含静态资源文件的问题。
2. 修改不同类型入口路径解析冲突的问题。

- **v0.0.5** 2022.01.07 `fix`

1. 修改 html 入口生成配置仍然是 js 文件路径的问题。
2. 支持 `root/src` 路径下配置 `manifest.(json|js)` 且优先级高于 `root`。
3. 支持自动处理热重载。
4. `background` 取消支持 html 类型入口。

- **v0.0.4** 2022.01.07 `fix`

1. 修复 manifest.json 中 js 类型入口路径解析不正确的问题。

- **v0.0.3** 2022.01.06

1. 修改 manifest.json 配置入口解析字段冲突的问题。

- **v0.0.2** 2022.01.06

1. 修改 manifest.json 配置入口解析不正确的问题。

- **v0.0.1** 2022.01.06

1. 实现 manifest.json 配置入口和约定式目录入口两种打包方案。
2. 实现 YAMLInJS 配置解析合并。
3. 实现 background、tabs、contextMenus、browserAction、pageAcition 等权限自动解析处理。
4. 实现 contextMenus 菜单配置化解析。

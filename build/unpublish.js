const { unpublishVersions } = require('./utils');

const [type = 'alpha', maxVersion = -1] = [process.argv[2], process.argv[3]];

async function unpublishVersions(type = 'alpha', maxVersion = -1) {
  if (!type) return console.log('请指定有效版本标识！');
  maxVersion = parseInt(maxVersion);

  const pkgName = 'ddd-do';
  let stdout = '';

  try {
    stdout = execSync(`npm view ${pkgName} versions`).toString('utf-8');
  } catch (error) {
    console.log('查看版本命令出错，请检查远端是否存在有效包！');
    return;
  }

  const versions = stdout.match(/(?<=\')\S+?(?=\')/g);
  if (versions.length < 1) return console.log('当前远端不存在有效版本包！');
  console.log(`当前有效版本列表：\n${version}`);

  const promises = versions.map(v => new Promise(resolve => {
    try {
      const typeIndex = v.indexOf(type);
      if (typeIndex < 1) resolve();

      const version = parseInt(v.substring(v.indexOf('.', typeIndex) + 1));
      if (maxVersion < 0 || version > maxVersion) resolve();

      console.log(`开始移除Version: ${v}`);
      execSync(`npm unpublish ${pkgName}@${v}`);
      console.log(`已移除Version: ${v}\n`);
    } catch (error) {
      console.log(`Unpublish Failed: ${v}`);
    }
    resolve();
  }));
  await Promise.all(promises);
  console.log(`已全部移除type为 ${type} 的包。`);
}

unpublishVersions(type, maxVersion);

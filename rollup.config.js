import json from 'rollup-plugin-json';
import ts from 'rollup-plugin-typescript2';
import commonjs from 'rollup-plugin-commonjs';
import packageJSON from './package.json';
import { resolve } from 'path';
import nodeResolve from '@rollup/plugin-node-resolve';

export default {
  input: './src/index.ts',
  output: [
    {
      name: 'nusuthPlugin',
      exports: 'named',
      file: packageJSON.main,
      globals: {
        fs: 'fs',
        path: 'path'
      },
      format: 'umd'
    }
  ],
  external: ['fs', 'path'],
  watch: {
    include: 'src/**/*'
  },
  plugins: [
    json(),
    commonjs(),
    nodeResolve(),
    ts({
      tsconfig: resolve(__dirname, './tsconfig.json'),
      exclude: ['*.js+(|x)', '**/*.js+(|x)'],
    }),
  ]
}

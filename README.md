# vite-plugin-crx-nusuth

> Hot-reload, reactive package tool for developing Chrome-Extension project.

[NPM](https://www.npmjs.com/package/vite-plugin-crx-nusuth)
[Bitbucket](https://bitbucket.org/I_Eki/vite-plugin-crx-nusuth)
[LICENSE](https://bitbucket.org/I_Eki/vite-plugin-crx-nusuth/src/master/LICENSE)

## Install

```bash
# install dependencies
npm i vite-plugin-crx-nusuth -D

# or use yarn
yarn add vite-plugin-crx-nusuth -D
```

## Usage

```javascript
import { defineConfig } from 'vite';
import vue from '@vitejs/plugin-vue';
import { nusuthPlugin } from 'vite-plugin-crx-nusuth';

export default defineConfig({
  plugins: [nusuthPlugin()],
});
```

## DOCS

_Todo_

## License

[MIT](https://bitbucket.org/I_Eki/vite-plugin-crx-nusuth/src/master/LICENSE).

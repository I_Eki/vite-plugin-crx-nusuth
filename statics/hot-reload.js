// The code is based on Chrome Extension Hot Reloader.
// Link: https://github.com/xpl/crx-hotreload
// Me: Eki <ichinoseeki@outlook.com>

const filesInDirectory = dir => new Promise (resolve =>
    dir.createReader ().readEntries (entries =>
        Promise.all (entries.filter (e => e.name[0] !== '.').map (e =>
            e.isDirectory
                ? filesInDirectory (e)
                : new Promise (resolve => e.file (resolve))
        ))
        .then (files => [].concat (...files))
        .then (resolve)
    )
)

// const timestampForFilesInDirectory = dir =>
//         filesInDirectory (dir).then (files =>
//             files.map (f => f.name + f.lastModifiedDate).join ())

// 当监听到manifest.json文件修改后，才更新时间戳（manifest.json文件被删除时也会触发）
const timestampForFilesInDirectory = (dir, lastTimestamp) => {
    return filesInDirectory (dir).then(files => {
        files.some (f => {
            if (f.name === 'manifest.json') {
                // 删除manifest.json文件时lastModified和删除前是一样的，可以据此排除这种情况
                if (lastTimestamp !== f.lastModified) {
                    lastTimestamp = f.lastModified
                }
                return true
            }
        })

        return lastTimestamp
    })
}

const watchChanges = (dir, lastTimestamp) => {
    timestampForFilesInDirectory (dir, lastTimestamp).then (timestamp => {
        if (!lastTimestamp || (lastTimestamp === timestamp)) {
            setTimeout (() => watchChanges (dir, timestamp), 1000) // retry after 1s
        } else {
            chrome.runtime.reload ()
        }
    })
}

chrome.management.getSelf (self => {
    if (self.installType === 'development') {
        chrome.runtime.getPackageDirectoryEntry (dir => watchChanges (dir))
        chrome.tabs.query ({ active: true, lastFocusedWindow: true }, tabs => { // NB: see https://github.com/xpl/crx-hotreload/issues/5
            if (tabs[0]) {
                chrome.tabs.reload (tabs[0].id)
            }
        })
    }
})
